<?php

function dtn_build_notification($transfer) {
	$notification = array();

	$notification['recipients'] = array();
	foreach($transfer['recipients'] as $recipient) {
		$notification['recipients'][] = array('type' => "email", 'id' => $recipient['email']);
	}

	$notification['files'] = array();
	foreach($transfer['files'] as $file) {
		$notification['files'][] = array(
			'type' => 'name',
			'id' => $file['uid'],
			'metadata' => array(
				'name' => $file['name'],
				'size' => $file['size']
			)
		);
	}

	$notification['sender'] = array('type' => "email", 'id' => $transfer['user_email']);

	$notification['frontend'] = array('type' => 'filesender', 'metadata' => array());
	$notification['frontend']['metadata'] = array(
		'subject' => $transfer['subject'],
		'message' => $transfer['message'],
		'expires' => $transfer['expires']['raw'],
		'options' => $transfer['options']
	);

	return $notification;
}

function dtn_send_notification($notification) {
	$dtn_agent_host = Config::get('dtn_agent_host');
	$dtn_agent_port = Config::get('dtn_agent_port');

	$options = array(
		CURLOPT_URL => "https://$dtn_agent_host:$dtn_agent_port/internal_notification",
		CURLOPT_POST => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPHEADER => array('Content-type: application/json'),
		CURLOPT_POSTFIELDS => json_encode($notification),
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_SSL_VERIFYHOST => 0,
	);

	error_log("[Info] Sending Notification to $dtn_agent_host:$dtn_agent_port\n");
	//error_log("[DEBUG] Internal Notification: " . json_encode($notification) . "\n";

	$curl = curl_init();
	curl_setopt_array($curl, $options);
	$response = curl_exec($curl);
	curl_close($curl);

	return $response;
}

$dtn_notification = dtn_build_notification(RestEndpointTransfer::cast($this));
$dtn_response = dtn_send_notification($dtn_notification);
$dtn_recipients = json_decode($dtn_response, true);

foreach($this->recipients as $recipient) {
	if ($dtn_recipients[$recipient->email]) {
		error_log("[INFO] DTN accepted recipient $recipient->email.\n");
	} else {
		error_log("[INFO] DTN rejected recipient $recipient->email.\n");
		$this->sendToRecipient('transfer_available', $recipient);
	}
}

?>
